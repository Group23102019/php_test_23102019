program Program_1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  Classes;

var
  LArray: TStringList;
  i: Integer;
begin
  try
    LArray := TStringList.Create;
    LArray.LineBreak:= ',';
    Randomize;
    for i := 1 to 100 do
      LArray.Add(IntToStr(Random(100) + 1));
    LArray.SaveToFile('numbers.csv');
  except
    on E: Exception do
    begin
      LArray.Free;
      Writeln(E.ClassName, ': ', E.Message);
      Readln;
    end;
  end;
end.
