<?php

abstract class Publication
{
    abstract public function getTitle();
    abstract public function getSource();
    abstract public function getContent();
    abstract public function getAll();
}

class News extends Publication
{
    private $title;
    private $text;
    private $link;

    public function __construct(string $title, string $text, string $link)
    {
        $this->title = $title;
        $this->text = $text;
        $this->link = $link;
    }
    public function getTitle(): string
    {
        return $this->title;
    }
    public function getSource(): string
    {
        return $this->link;
    }
    public function getContent(): string
    {
        return $this->text;
    }
    public function getAll() {}
}

class Announce extends Publication
{
    private $title;
    private $text;
    private $author;

    public function __construct(string $title, string $text, string $author)
    {
        $this->title = $title;
        $this->text = $text;
        $this->author = $author;
    }
    public function getTitle(): string
    {
        return $this->title;
    }
    public function getSource(): string
    {
        return $this->author;
    }
    public function getContent(): string
    {
        return $this->text;
    }
    public function getAll() {}
}

class MySQLConnection
{
    private static $instance;
    private $db = 'mysql::host=localhost;dbname=my_db';
    private $username = 'testuser';
    private $password = 'testUSER!123';
    private $connection;

    private function __construct() {}

    private static function getInstance()
    {
        if (self::$instance == null)
        {
            $className = __CLASS__;
            self::$instance = new $className;
        }
        return self::$instance;
    }

    private static function init()
    {
        $dbConnect = self::getInstance();
        if ($dbConnect->connection == null)
        {
            $dbConnect->connection = new PDO($dbConnect->db, $dbConnect->username, $dbConnect->password);
        }
        return $dbConnect;
    }

    public static function query(string $queryString, array $queryParams, array $queryParamsType): array
    {
        $dbConnect = self::init();
        $queryExe = $dbConnect->connection->prepare($queryString);
        foreach ($queryParams as $paramName => $paramValue)
        {
            $queryExe->bindValue(":$paramName", $paramValue, $queryParamsType[$paramName]);
        }
        $queryExe->execute();
        $resultArray = $queryExe->fetchAll(PDO::FETCH_ASSOC);
        return $resultArray;
    }

    public static function execute(string $queryString, array $queryParams, array $queryParamsType): void
    {
        $dbConnect = self::init();
        $queryExe = $dbConnect->connection->prepare($queryString);
        foreach ($queryParams as $paramName => $paramValue)
        {
            $queryExe->bindValue(":$paramName", $paramValue, $queryParamsType[$paramName]);
        }
        $queryExe->execute();
    }
}

class NewsDB
{
    public function __construct()
    {
        $check = MySQLConnection::query("select count(*)  cnt
                                                    from information_schema.tables 
                                                    where table_schema = 'my_db' 
                                                        and table_name = 'news'", [], []);
        if ($check[0]['cnt'] == 0)
        {
            MySQLConnection::execute("create table my_db.news (
                                                id int(10) not null auto_increment, 
                                                title varchar(50) not null, 
                                                text text not null,
                                                link varchar(100) not null,
                                                created_at datetime default current_timestamp,
                                                updated_at datetime default current_timestamp,
                                                primary key(id))", [], []);
        }
    }

    public function create(string $title, string $text, string $source): News
    {
        MySQLConnection::execute('insert into my_db.news (title, text, link) values (:title, :text, :link)',
                                    ['title' => $title, 'text' => $text, 'link' => $source],
                                    ['title' => PDO::PARAM_STR, 'text' => PDO::PARAM_STR, 'link' => PDO::PARAM_STR]);
        return new News($title, $text, $source);
    }

    public function all(): array
    {
        $array = MySQLConnection::query('select title, text, link from my_db.news', [], []);
        $i = 0;
        foreach ($array as $element)
        {
            $newsArray[$i] = new News($element['title'], $element['text'], $element['link']);
            $i++;
        }
        return $newsArray;
    }
}
class AnnounceDB
{
    public function __construct()
    {
        $check = MySQLConnection::query("select count(*)  cnt
                                                    from information_schema.tables 
                                                    where table_schema = 'my_db' 
                                                        and table_name = 'announces'", [], []);
        if ($check[0]['cnt'] == 0)
        {
            MySQLConnection::execute("create table my_db.announces (
                                                id int(10) not null auto_increment, 
                                                title varchar(50) not null, 
                                                text text not null,
                                                author varchar(100) not null,
                                                created_at datetime default current_timestamp,
                                                updated_at datetime default current_timestamp,
                                                primary key(id))", [], []);
        }
    }

    public function create(string $title, string $text, string $source): Announce
    {
        MySQLConnection::execute('insert into my_db.announces (title, text, author) values (:title, :text, :author)',
                                        ['title' => $title, 'text' => $text, 'author' => $source],
                                        ['title' => PDO::PARAM_STR, 'text' => PDO::PARAM_STR, 'author' => PDO::PARAM_STR]);
        return new Announce($title, $text, $source);
    }

    public function all(): array
    {
        $array = MySQLConnection::query('select title, text, author from my_db.announces', [], []);
        $i = 0;
        foreach ($array as $element)
        {
            $announcesArray[$i] = new Announce($element['title'], $element['text'], $element['author']);
            $i++;
        }
        return $announcesArray;
    }
}


function getNewString(int $length)
{
    $symbols = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя, .!?';
    $name = mb_strtoupper(mb_substr($symbols, mt_rand(0, 32), 1));
    for ($i = 0; $i < $length- 2; $i++)
    {
        $name .= mb_substr($symbols, mt_rand(0, 34), 1);
    }
    $name .= mb_substr($symbols, mt_rand(35, 37), 1);
    return $name;
}

MySQLConnection::execute('delete from my_db.news', [], []);
MySQLConnection::execute('delete from my_db.announces', [], []);

$newsDB = new NewsDB();
$announceDB = new AnnounceDB();

for ($i = 0; $i < 100; $i++)
{
    $newsDB->create('Заголовок новости_'.($i + 1), getNewString(50), getNewString(10));
    $announceDB->create('Заголовок объявления_'.($i + 1), getNewString(50), getNewString(10));
}

$newsArray = $newsDB->all();
$announceArray = $announceDB->all();

$publicationArray = $newsArray;
$newsLength = count($newsArray);
for ($i = 0; $i < count($announceArray); $i++)
{
    $publicationArray[$i + $newsLength] = $announceArray[$i];
}

for ($i = 0; $i < count($publicationArray); $i++)
{
    echo $publicationArray[$i]-> getTitle(), " ",
            $publicationArray[$i]-> getContent(), " ",
            $publicationArray[$i]-> getSource(), "<br>";
}
