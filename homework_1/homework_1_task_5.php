<?php

function processArray(array $array, $string)
{
	if (($array <> null) and ($string <> null))
	{
		$i = 0;
		foreach($array as $element)
		{
			$result[$i] =  $element.$string;
			$i++;
		}
	}
	else
	{
		$result = false;
	}
	return $result;
}

$array = processArray([1, 2, 3, 4, 5], 6);

if ($array <> null)
{
	foreach($array as $element)
	{
		echo $element, "\n";
	}
};