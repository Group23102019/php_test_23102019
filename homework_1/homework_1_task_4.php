<?php

$date1 = strtotime("2019-08-01");
$date2 = $date1 + date("t", strtotime($date1))*24*60*60;

while ($date1 <= $date2)
{
	if (date('w', $date1) == 1)
	{
		echo date('d-m-Y', $date1), "\n";
	};
	$date1 = $date1 + 1*24*60*60;
};