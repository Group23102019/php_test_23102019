<?php

function getTuesdays(int $year, int $month)
{
	if (($year > 0) and ($month > 0) and ($month <= 12))
	{
		$date1 = strtotime($year . '-' . $month . '-01');
		$date2 = $date1 + date("t", strtotime($date1)) * 24*60*60;
		while ($date1 <= $date2)
		{
			if (date('w', $date1) == 2)
			{
				echo date('d-m-Y', $date1), "\n";
			};
			$date1 = $date1 + 1*24*60*60;
		}
	}
	else
	{
		echo "Wrong date!";
	}
}

getTuesdays(2019, 8);