<?php

define(maxRecLevel, 15);

function calculate(int $a, int $b, int $num=0, int $sum=0, int $last=0, bool $isBoth=false, int $level=0)
{
 $result = $num;
 if ((($num % $a <> 0) or ($sum % $a <> 0) or ($num % $b <> 0) or ($sum % $b <> 0) or !$isBoth or ($num == 0)) and ($level <= maxRecLevel))
 {
  $result = min(calculate($a, $b, $num * 10 + $a, $sum + $a, $a, ($isBoth or ($last == $b)), $level + 1),
    			calculate($a, $b, $num * 10 + $b, $sum + $b, $b, ($isBoth or ($last == $a)), $level + 1)); 
 };
 return $result;
};

echo calculate(3, 7);