program Program_3;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  Classes;

function Factorial(ANumber: Integer): Integer;
begin
  Result := 1;
  if ANumber > 0 then
    Result :=  ANumber * Factorial(ANumber - 1);
end;

var
  LNumber: Integer;
begin
  try
    Readln(LNumber);
    if (LNumber > 0) and (LNumber <= 10) then
      Writeln(IntToStr(Factorial(LNumber)))
    else
      Writeln('����� �� ��������');
    Readln;
  except
    on E: Exception do
    begin
      Writeln(E.ClassName, ': ', E.Message);
      Readln;
    end;
  end;
end.
