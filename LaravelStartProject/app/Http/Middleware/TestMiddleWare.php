<?php

namespace App\Http\Middleware;

use Closure;

class TestMiddleWare
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $apiKey = env('API_KEY', '');
        if ($request->header('x_api_key') == $apiKey)
        {
            return $next($request);
        }
        else
        {
            return responce() -> json([], 401);
        }
    }
}
