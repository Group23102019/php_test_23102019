<?php

namespace App\Http\Controllers;

use App\Exceptions\TestException;
use App\Facades\TestBasicServiceFacade;
use Illuminate\Http\Request;
use App\Test;

class TestController extends Controller
{
    private $testBasicService;

    public function _construct(TestBasicServiceFacade $testBasicService)
    {
        $this->testBasicService = $testBasicService;
    }
    public function getOne()
    {
        return response()->json($this->testBasicService::GetAttributes(), 200);
    }
    public function setOne(Request $request)
    {
        $newTestOne = new TestBasicServiceFacade();
        $newTestOne::setName($request->name);
        $newTestOne::setAge($request->age);
        $newTestOne::setLiving($request->state);
        return response()->json($newTestOne::GetAttributes(), 200);
    }
    public function findTest(Request $request)
    {
        $test = Test::findTestById($request->id);
        return response()->json($test, 200);
    }
    public function findTestByText(Request $request)
    {
        $tests = Test::findTestByText($request->string);
        return response()->json($tests, 200);
    }
    public function myFirstTest(Request $request)
    {
        return response('my first test', 200);
    }
}
