<?php


namespace App\Services;

use App\Exceptions\TestException;
use App\Test;
use Illuminate\Http\Request;

class TestOne
{
    private $name;
    private $age;
    private $state;
}


class TestBasicService
{
    private $name;
    private $age;
    private $state;
    private $testOne;

    public function _construct(TestOne $testOne)
    {
        $this->testOne = $testOne;
    }
    public function setName(string $newName)
    {
        $this->name = $newName;
    }
    public function setAge(int $newAge)
    {
        $this->age = $newAge;
    }
    public function setLiving(int $newState)
    {
        $this->state = $newState;
    }
    public function getAttributes()
    {
        return ['name' => $this->name, 'age' => $this->age, 'state' => $this->state];
    }
    public function getException(int $id)
    {
        $test = Test::findTestById($id);
        if ($test == null)
        {
            throw new TestException();
        }
    }
    public function getForMock(string $var): string
    {
        return $var;
    }
}
