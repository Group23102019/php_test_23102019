<?php


class TestOne
{
    private $name;
    private $age;
    private $state;

    public function setName(string $newName)
    {
        $this->name = $newName;
    }
    public function setAge(int $newAge)
    {
        $this->age = $newAge;
    }
    public function setLiving(int $newState)
    {
        $this->state = $newState;
    }
    public function getAttributes()
    {
        return ['name' => $this->name, 'age' => $this->age, 'state' => $this->state];
    }
}
