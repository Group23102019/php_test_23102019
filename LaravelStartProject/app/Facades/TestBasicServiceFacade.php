<?php


namespace App\Facades;
use Illuminate\Support\Facades\Facade;

/**
 * @method static setName(string $newName)
 * @method static setAge(int $newAge)
 * @method static setLiving(int $newState)
 * @method static getAttributes()
 * @method static getForMock(string $var)
 * @see \App\Services\TestBasicService;
 */

class TestBasicServiceFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'service.test_basic';
    }
}
