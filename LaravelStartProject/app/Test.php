<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use tests\Mockery\Generator\ClassWithDebugInfo;

class Test extends Model
{
    protected $fillable;
    protected $cast =['is_enable' => 'boolean'];

    public static function setPepito()
    {
        $pepito_old = Test::latest('id')-> first();
        $pepito_new = $pepito_old;
        $pepito_new -> text = 'So what about Pepito?';
        $pepito_new -> is_enabled = 1;
        return $pepito_old;
    }

    public static function findTestById(int $id)
    {
        $test = Test::where('id', $id)->first();
        if ($test <> null)
        {
            return $test;
        }
        else
        {
            return null;
        }
    }

    public static function findTestByText(string $text)
    {
        $tests = Test::where('text', $text)->get();
        if (count($tests) <> 0)
        {
            return $tests;
        }
        else
        {
            return null;
        }
    }

}
