<?php

namespace Tests\Feature;

use App\Facades\TestBasicServiceFacade;
use App\Services\TestBasicService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class facadeTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        TestBasicServiceFacade::shouldReceive('getForMock')->once()->andReturn('my third test');
        $result = TestBasicServiceFacade::getForMock('my third test');
        $this->assertEquals($result, 'my third test');
    }
}
