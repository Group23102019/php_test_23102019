program Program_2;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  Classes;

var
  LArray: TStringList;
  i: Integer;
  ACount: Integer;
begin
  try
    LArray := TStringList.Create;
    LArray.LineBreak:= ',';
    LArray.LoadFromFile('numbers.csv');
    ACount := 0;
    for i := 0 to LArray.Count - 1 do
    begin
      Write(LArray[i]);
      if LArray[i].ToInteger() > 50 then
      begin
        Writeln(' - 1');
        Inc(ACount);
      end
      else
        Writeln(' - 0');
    end;
    Writeln('����� ����� ������ 50 ' + IntToStr(ACount));
    Readln;
  except
    on E: Exception do
    begin
      LArray.Free;
      Writeln(E.ClassName, ': ', E.Message);
      Readln;
    end;
  end;

end.
