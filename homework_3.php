<?php

function getNewName()
{
    $symbols = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя';
    $nameLength = mt_rand(5, 10);
    $name = mb_strtoupper(mb_substr($symbols, mt_rand(0, 32), 1));
    for ($i = 0; $i < $nameLength - 1; $i++)
    {
        $name .= mb_substr($symbols, mt_rand(0, 32), 1);
    }
    return $name;
}

function getNewAge()
{
    return mt_rand(10, 100);
}

function addUser($pdo, string $name, int $age)
{
    $sql = "INSERT INTO my_db.users (name, age) values (:name, :age)";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':age', $age);
    $stmt->execute();
}

function addUsers($pdo, int $usersCount)
{
    for ($i = 0; $i < $usersCount; $i++)
    {
        addUser($pdo, getNewName(), getNewAge());
    }
}

function selectQuery1($pdo)
{
    $query = $pdo->query("select name, age from my_db.users where age > 50");
    $array = $query->fetchAll(PDO::FETCH_ASSOC);
    return $array;
}

function selectQuery2($pdo)
{
    $query = $pdo->query("select name, age from my_db.users where lower(name) like '%аб%' or lower(name) like '%ав%'");
    $array = $query->fetchAll(PDO::FETCH_ASSOC);
    return json_encode($array, JSON_UNESCAPED_UNICODE);
}

function selectQuery3($pdo)
{
    $query = $pdo->query("select name, age from my_db.users where age > 70");
    $array = $query->fetchAll(PDO::FETCH_ASSOC);

    $queryExe = $pdo->prepare("update my_db.users set name = 'Pepito' where age > 70");
    $queryExe->execute();
    return $array;
}

function printQuery(array $array)
{
    foreach($array as $row)
    {
        foreach($row as $column)
        {
            echo $column." ";
        }
        echo '<br>';
    }
}

function selectQuery4($pdo)
{
    $query = $pdo->query("select distinct name from my_db.users");
    $array = $query->fetchAll(PDO::FETCH_ASSOC);
    printQuery($array);
}


$pdo = new PDO("mysql::host=localhost;dbname=my_db", 'testuser', 'testUSER!123');
$sql = "delete from my_db.users";
$stmt = $pdo->prepare($sql);
$stmt -> execute();

//задание 1
addUsers($pdo, 1000);

//задание 2
$oldmans = selectQuery1($pdo);
//printQuery($oldmans);

//задание 3
$json = selectQuery2($pdo);
//echo $json;

//задание 4
$pepitos = selectQuery3($pdo);
//printQuery($pepitos);

//задание 5
selectQuery4($pdo);
